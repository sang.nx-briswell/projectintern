<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Orders', [
            'foreignKey' => 'user_id',
        ]);
        $this->addBehavior('Update');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 256)
            ->notEmptyString('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->notEmptyString('password');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 50)
            ->allowEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 50)
            ->allowEmptyString('last_name');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 10)
            ->allowEmptyString('phone');

        $validator
            ->scalar('address')
            ->maxLength('address', 256)
            ->allowEmptyString('address');

        $validator
            ->scalar('avatar')
            ->maxLength('avatar', 256)
            ->allowEmptyString('avatar');

        $validator
            ->integer('user_flag')
            ->notEmptyString('user_flag');

        $validator
            ->allowEmptyString('created_by');

        $validator
            ->dateTime('created_at')
            ->allowEmptyDateTime('created_at');

        $validator
            ->allowEmptyString('updated_by');

        $validator
            ->dateTime('updated_at')
            ->allowEmptyDateTime('updated_at');

        $validator
            ->allowEmptyString('deleted_by');

        $validator
            ->dateTime('deleted_at')
            ->allowEmptyDateTime('deleted_at');

        $validator
            ->date('birth')
            ->allowEmptyDate('birth');

        $validator
            ->scalar('city')
            ->maxLength('city', 256)
            ->allowEmptyString('city');

        $validator
            ->scalar('district')
            ->maxLength('district', 256)
            ->allowEmptyString('district');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }

    // get user info
    public function getListUserInfo() {
        $query = $this->find()->all();
        return $query;
    }

    // get user info by id
    public function getUserInfoByID($id) {
        $query = $this->get($id);
        return $query;
    }

    // get user info by email
    public function getUserInfoByEmail($param){
        $query = $this->find()
            ->where(['username' => $param]);
        return $query;
    }

    // get user info by data from search form
    public function searchUserInfo($condition = [])
    {
        $query = $this->find()->select($this)->where(['1=1']);
        // first name not null
        if (!empty($condition["first_name"])) {
            $query->andWhere(['Users.first_name' => $condition["first_name"]]);
        }
        // email not null
        if (!empty($condition["email"])) {
            $query->andWhere(['Users.username' => $condition["email"]]);
        }
        // amount from and amount to not null
        if (!empty($condition["amount_from"]) || !empty($condition["amount_to"])) {
            $query->select([
                'ord_dt.total'
            ])->join([
                'table' => 'orders',
                'alias' => 'ord',
                'type' => 'INNER',
                'conditions' => 'ord.user_id = users.id'
            ])->Join([
                'table' => 'order_details',
                'alias' => 'ord_dt',
                'type' => 'INNER',
                'conditions' => 'ord_dt.order_id = ord.id'
                ]);
            // amount from not null
            if (!empty($condition["amount_from"])) {
                $query->andWhere([
                    'ord_dt.total >=' => $condition["amount_from"],
                ]);
            }
            // amount to not null
            if (!empty($condition["amount_to"])) {
                $query->andWhere([
                    'ord_dt.total <=' => $condition["amount_to"],
                ]);
            }
        }
        return $query;
    }
}
