<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderDetail Entity
 *
 * @property int $id
 * @property int $amount
 * @property int $total
 * @property int $product_id
 * @property int $order_id
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property int|null $updated_by
 * @property \Cake\I18n\FrozenTime|null $updated_at
 * @property int|null $deleted_by
 * @property \Cake\I18n\FrozenTime|null $deleted_at
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Order $order
 */
class OrderDetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'total' => true,
        'product_id' => true,
        'order_id' => true,
        'created_by' => true,
        'created_at' => true,
        'updated_by' => true,
        'updated_at' => true,
        'deleted_by' => true,
        'deleted_at' => true,
        'product' => true,
        'order' => true,
    ];
}
