<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $product_name
 * @property string $product_info
 * @property string $product_image
 * @property int|null $price
 * @property int|null $amount
 * @property int $top_flg
 * @property int $category_id
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property int|null $updated_by
 * @property \Cake\I18n\FrozenTime|null $updated_at
 * @property int|null $deleted_by
 * @property \Cake\I18n\FrozenTime|null $deleted_at
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\OrderDetail[] $order_details
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_name' => true,
        'product_info' => true,
        'product_image' => true,
        'price' => true,
        'amount' => true,
        'top_flg' => true,
        'category_id' => true,
        'created_by' => true,
        'created_at' => true,
        'updated_by' => true,
        'updated_at' => true,
        'deleted_by' => true,
        'deleted_at' => true,
        'category' => true,
        'order_details' => true,
    ];
}
