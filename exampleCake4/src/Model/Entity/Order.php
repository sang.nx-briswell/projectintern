<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property string|null $note
 * @property int $user_id
 * @property int|null $created_by
 * @property \Cake\I18n\FrozenTime|null $created_at
 * @property int|null $updated_by
 * @property \Cake\I18n\FrozenTime|null $updated_at
 * @property int|null $deleted_by
 * @property \Cake\I18n\FrozenTime|null $deleted_at
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\OrderDetail[] $order_details
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'note' => true,
        'user_id' => true,
        'created_by' => true,
        'created_at' => true,
        'updated_by' => true,
        'updated_at' => true,
        'deleted_by' => true,
        'deleted_at' => true,
        'user' => true,
        'order_details' => true,
    ];
}
