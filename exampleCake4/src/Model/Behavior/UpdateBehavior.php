<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\Utility\Text;

/**
 * Update behavior
 */
class UpdateBehavior extends Behavior
{
    protected $_defaultConfig = [
    ];
    /**
     * Before update event
     *
     * @param Event $event
     * @param EntityInterface $entity
     * @param \ArrayObject $options the options passed to the save method
     */
    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, \ArrayObject $options) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $now = date("Y/m/d h:i:s", time());
        if($entity->isNew()) {
            $entity->set('created_at', $now);
            $entity->set('updated_at', $now);
        } else {
            $entity->set('updated_at', $now);
        }
    }
}
