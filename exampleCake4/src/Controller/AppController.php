<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use Cake\Event\Event;
use App\Libs\ConfigUtil;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Auth', [
            'userModel' => 'Users',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Products',
                'action' => 'index'
            ],
            'authError' => 'Did you really think you are allowed to see that?',
            'authorize' => 'Controller',
            'unauthorizedRedirect' => $this->referer(),
            'storage' => 'Session'
        ]);

        //Allow guest access to login and register page
        $this->Auth->allow(['login', 'register', 'ajaxDistrictByCity', 'permissionError']);
        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');
    }

    public function isAuthorized($user = null) {
        $firewallList = [
            'DEFAULT' => [],
            'ADMIN' => [
                'Users' => ['listUser', 'view', 'edit', 'export', 'logout', 'clearDataForm'],
                'Products' => ['index', 'add', 'edit', 'list', 'view'],
            ],
            'STAFF' => [
                'Users' => ['logout'],
                'Products' => ['index', 'list', 'add', 'edit'],
            ],
            'USER' => [
                'Users' => ['logout'],
                'Products' => ['index'],
            ],
        ];
        $roleArr = ['ADMIN', 'STAFF', 'USER'];
        $role = $roleArr[$user['user_flag']];
        //Check roles
        if($user === null || empty($role)){
            $firewall  = $firewallList['DEFAULT'];
        } else {
            $firewall = $firewallList[$role];
        }
        //Get controller and action parameter
        $controller = $this->request->getParam('controller');
        $action = $this->request->getParam('action');
        //Check screen permission
        if($firewall && array_key_exists($controller, $firewall)){
            if($firewall[$controller] === null || in_array($action, $firewall[$controller])){
                return true;
            } else {
                //Redirect to error page
                return $this->redirect(['controller' => 'Top', 'action' => 'permissionError']);
            }
        }
        //If can't go to page, redirect to error page
        return $this->redirect(['controller' => 'Top', 'action' => 'permissionError']);
    }

    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);
    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $user = $this->Auth->user();
        if($user) {
            $this->set(compact('user'));
        }
    }
}
