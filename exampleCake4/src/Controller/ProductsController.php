<?php
declare(strict_types=1);

namespace App\Controller;

use App\Libs\ConfigUtil;
use App\Model\Table\CategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\Event\EventInterface;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index() {
        $this->set('products', $this->paginate($this->Products, ['limit' => 6]));
    }

    /**
     * List products method
     *
     * @return \Cake\Http\Response|null
     */
    public function list() {
        $this->paginate = [
            'contain' => ['Categories'],
        ];
        $products = $this->Products->find('all')->contain('categories');
        $products = $this->paginate($products,  ['limit' => 5]);
        $this->set('products', $products);
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $product = $this->Products->get($id);
        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $Categories = $this->loadModel('Categories');
        $categories = $Categories->find('all')->extract('category_name');
        $this->set(compact( 'categories'));
        $product = $this->Products->newEmptyEntity();
        if ($this->request->is('post')) {
            $dataForm = $this->request->getData();
            $image = $dataForm["product_image"];
            $target_file = "/img/product/" . $image->getClientFileName();
            if (move_uploaded_file($_FILES["product_image"]["tmp_name"], WWW_ROOT . $target_file)) {
                $dataForm["category_id"] += 1;
                $dataForm["product_image"] = $target_file;
                $product = $this->Products->patchEntity($product, $dataForm);
                if ($this->Products->save($product)) {
                    $this->Flash->success(__('The product has been saved.'));
                    return $this->redirect(['action' => 'list']);
                }
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null) {
        $product = $this->Products->get($id, [
            'contain' => ['Orders', 'Categories'],
        ]);
        $Categories = $this->loadModel('categories');
        $categories = $Categories->find('all')->extract('category_name');
        $this->set(compact('product', 'categories'));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dataForm = $this->request->getData();
            // Check chose image
            if(!empty($dataForm["product_image"]->getClientFileName())) {
                $image = $dataForm["product_image"];
                $target_file = "/img/product/" . $image->getClientFileName();
                if(move_uploaded_file($_FILES['product_image']['tmp_name'], WWW_ROOT.$target_file)) {
                    $dataForm["product_image"] = $target_file;
                    $dataForm["category_id"] += 1;
                    $product = $this->Products->patchEntity($product, $dataForm);
                    if ($this->Products->save($product)) {
                        $this->Flash->success(__('The product has been saved.'));
                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The product could not be saved. Please, try again.'));
                }
            } else {
                $dataForm['product_image'] = $product['product_image'];
                $dataForm["category_id"] += 1;
                $product = $this->Products->patchEntity($product, $dataForm);
                if ($this->Products->save($product)) {
                    $this->Flash->success(__('The product has been saved.'));
                    return $this->redirect(['action' => 'list']);
                }
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
