<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * user Login
     *
     * @return \Cake\Http\Response|null
     */
    public function login() {
        // Check logged
        if ($this->Auth->user()){
            $this->redirect('/product');
        } else {
            $this->set('title','Login');
            if ($this->request->is('post')){
                $user = $this->Auth->identify();
                if ($user){
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->error('Login Fail!');
            }
        }
    }

    /**
     * User Register
     *
     * @return \Cake\Http\Response|null
     */
    public function register() {
        $cities = $this->getCity();
        $districts = $this->getDistrict();
        $this->set(compact(['cities', 'districts']));
        $this->set('title', 'Register');
        if($this->request->is('post')) {
            $param = $this->request->getData();
            if (empty($this->Users->getUserInfoByEmail($param["username"])->toArray())) {
                $data = $this->Users->newEntity($this->request->getData());
                if($this->Users->save($data)) {
                    $this->Flash->success('Register success!');
                    return $this->redirect(['control' => 'Users', 'action' => 'login']);
                }
                $this->Flash->error('Register fail!');
            }
            $this->Flash->error('Email is exist!');
        }
    }

    /**
     *  user logout
     *
     * @return \Cake\Http\Response|null
     */
    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * List User Info
     *
     * @return \Cake\Http\Response|null
     */
    public function listUser()
    {
        $session = $this->getRequest()->getSession();
        $params = [];
        if($session->check('Users.listUser')) {
            $params = $session->read('Users.listUser');
            $this->set([
                'searchData' => $params,
            ]);
        }
        // Paginate data from table Users
        if ($this->request->is('post')) {
            // method import file csv
            $this->import();
            $searchData = $this->getRequest()->getData();
            //Write session
            $session->delete('Users.listUser');
            $session->write('Users.listUser', $searchData);
            return $this->redirect(['action' => 'listUser']);
        }
        $usersQuery = $this->Users->searchUserInfo($params);
        $usersInfo = $this->paginate($usersQuery, ['limit' => 5]);
        $this->set('users', $usersInfo);
    }

    /**
     * Clear data in form search
     *
     * @return \Cake\Http\Response|null
     */
    public function clearDataForm() {
        $session = $this->request->getSession();
        if($session->check('Users.listUser')) {
            $session->delete('Users.listUser');
            exit();
        }
    }

    /**
     * List city
     *
     * @return \Cake\Http\Response|array
     */
    private function getCity() {
        return ['Ho Chi Minh', 'Ha Noi', 'Da Nang'];
    }

    /**
     * List district
     *
     * @return \Cake\Http\Response|array
     */
    private function getDistrict($country_id = null) {
        switch ($country_id)
        {
            case 0:
                $states = ['District 1', 'District 2', 'District 3'];
                break;
            case 1:
                $states = ['Cau Giay', 'Thanh Xuan'];
                break;
            case 2:
                $states = ['District 4', 'District 5', 'District 6'];
                break;
            default:
                $states = [];
        }
        return $states;
    }

    /**
     * Get ajax district by city
     *
     * @return \Cake\Http\Response|json
     */
    public function ajaxDistrictByCity() {
        $id = $this->request->getData('id');
        $states = ['data' => ['districts' => $this->getDistrict($id)]];
        echo json_encode($states);
        exit();
    }

    /**
     * User Edit
     *
     * @return \Cake\Http\Response|null
     */
    public function view($id) {
        $title = 'User Info';
        $userTable = TableRegistry::getTableLocator()->get('Users');
        $user = $userTable->get($id);
        $this->set(compact('user', 'title'));
    }

    /**
     * User Edit
     *
     * @return \Cake\Http\Response|null
     */
    public function edit($id = null) {
        $this->set('title', 'Edit');
        $cities = $this->getCity();
        $districts = $this->getDistrict();
        $this->set(compact(['cities', 'districts']));
        // find user by id
        $userTable = TableRegistry::getTableLocator()->get('Users');
        $user = $userTable->get($id);
        // Check exist data
        if($this->request->is('post')) {
            $dataForm = $this->request->getData();
            // Check password empty
            if(isset($dataForm["password_edit"]) && !empty($dataForm["password_edit"])) {
                $dataForm["password"] = $dataForm["password_edit"];
            }
            // Check upload file
            $image = $dataForm["avatar"];
            $target_file = "/img/user/" . $image->getClientFilename();
            // check chose image
            if($image->getSize() > 0) {
                $dataForm["avatar"] = $target_file;
                if ($image->getError() == 0) {
                    // Move file to webroot/img/user
                    if (move_uploaded_file($_FILES["avatar"]["tmp_name"], WWW_ROOT . $target_file)) {
                        $obj = $this->Users->patchEntity($user, $dataForm);
                        // Save data
                        if ($this->Users->save($obj)) {
                            $this->Flash->success("Edit success!");
                            return $this->redirect('/user');
                        }
                        $this->Flash->error("Edit fail!");
                    }
                }
                $this->Flash->error("Upload fail!");
            } else {
                // Change avatar in form to avatar in database
                $dataForm["avatar"] = $user["avatar"];
                $obj = $userTable->patchEntity($user, $dataForm);
                if ($this->Users->save($obj)) {
                    $this->Flash->success("Edit success!");
                    return $this->redirect('/user');
                }
                $this->Flash->error("Edit fail!");
            }
        }
        $this->set('data', $user);
    }

    /**
     * Export csv
     *
     * @return \Cake\Http\Response|null
     */
    public function export() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $date = date('Y/mm/dd h:i:s', time());
        $this->response->withDownload('userinfo.csv');
        $data = $this->Users->find('all')->toArray();
        $_serialize = 'data';
        $_header = ['ID', 'Email', 'First_Name', 'Last_Name', 'Phone_Number', 'Address', 'Birth'];
        $this->viewBuilder()->setClassName('CsvView.Csv');
        $this->set(compact('data', '_serialize', '_header'));
        return;
    }

    public function import() {
        if(!empty($_FILES['upload_file']["name"])) {
            $file = $_FILES['upload_file']['tmp_name'];
            $handle = fopen($file, "r");
            while(($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($row[0] == 'ID') {
                    continue;
                }
                $column = [
                    'username' => $row[1],
                    'first_name' => $row[2],
                    'last_name' => $row[3],
                    'phone' => $row[4],
                    'address' => $row[5],
                    'avatar' => null,
                    'birth' => date("Y/m/d", (int)strtotime((string)$row[13]))
                ];
                $obj = $this->Users->newEntity($column);
                $this->Users->save($obj);
            }
            fclose($handle);
            $this->Flash->success("Import success!");
        }
    }
}
