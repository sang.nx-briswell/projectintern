$(document).ready(function () {
    $('#formLogin').validate({
        rules: {
            "username": {
                required: true,
                email: true,
            },
            "password": {
                required: true,
            }
        },
        messages: {
            "username": {
                required: "please enter your email",
                email: "email not correct",
            },
            "password": "please enter your password",
        }
    });
});

