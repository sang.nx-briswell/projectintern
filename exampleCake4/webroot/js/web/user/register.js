$(document).ready(function () {
    $('#formRegister').validate({
        rules: {
            username: {
                required: true,
                email: true,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 50
            },
            first_name: {
                required: true,
                maxlength: 50
            },
            last_name: {
                required: true,
                maxlength: 50,
            },
            city: {
                required: true,
            },
            district: {
                required: true,
            },
            address: {
                required: true,
                maxlength: 255
            },
            phone: {
                required: true,
                maxlength:10,
                digits: true
            }
        },
        messages: {
            username: {
                required: "please enter your email",
                email: "email is not correct"
            },
            password: {
                required: "please enter your password"
            },
            first_name: {
                required: "please enter your name"
            },
            last_name: {
                required: "please enter your name"
            },
            city: {
                required: "please select city"
            },
            district: {
                required: "please select district"
            },
            address: {
                required: "please enter your address"
            },
            phone: {
                required: "please enter your phone"
            }
        }
    });
});
