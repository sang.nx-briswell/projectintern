$(document).ready(function () {
    $('#formEdit').validate({
        rules: {
            "username": {
                required: true,
                email: true,
                maxlength: 50
            },
            "password_edit" : {
                minlength: 6,
                maxlength: 50
            },
            "first_name": {
                required: true,
                maxlength: 50
            },
            "last_name": {
                required: true,
                maxlength: 50
            },
            "phone": {
                maxlength: 10,
                number: true
            },
            "city": {
                required: true,
            },
            "district": {
                required: true,
            },
            "address": {
                required: true,
                maxlength: 255
            },
            "birth": {
                required: true,
                date: "yy-mm-dd"
            },
            "avatar": {
                extension: "png|jpe?g|gif|gif",
            }
        },
        messages: {
            "username": {
                required: "please enter your email",
                email: "email is not correct",
            },
            "password": {
                minlength: "pass more than 5"
            },
            "first_name": {
                required: "please enter your name"
            },
            "last_name": {
                required: "please enter your name"
            },
            "city": {
                required: "please select city"
            },
            "district": {
                required: "please select district"
            },
            "address": {
                required: "please enter your address"
            },
            "birth": {
                required: "please enter your birth",
            },
            "avatar": {
                extension: "Not an image!",
            }
        }
    });
});

// Datepicker
$( function() {
    $("#birth").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        minDate: new Date("1950/01/01"),
        maxDate: "-1y"
    });
});





