$(document).ready(function () {
    $('#clear').click(function () {
        $('#first-name').val("");
        $('#amount-from').val("");
        $('#amount-to').val("");
        $('#email').val("");
        var urlClearParams = 'users/clearDataForm';
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: urlClearParams,
            data: null,
            cache: false,
            success: function() {
            }
        })
    })
});
