$(document).ready(function () {
    $('#cities_field').change(function () {
        var country_id = $(this).val();
        var url = $("#get_country_url").val();
        $("#districts_field").find('option').remove();
        if(country_id) {
            var dataString = 'id='+country_id;
            $.ajax({
                dataType: 'json',
                type: "post",
                url: url,
                data: dataString,
                cache: false,
                success: function(data) {
                    $.each(data['data']['districts'], function(key, value) {
                        $('<option>').val(key).text(value).appendTo($("#districts_field"));
                    });
                }
            });
        }
    });
});
