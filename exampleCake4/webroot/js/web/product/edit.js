$(document).ready(function () {
    $("#edit_product").validate({
        rules: {
            product_name: {
                required: true,
                maxlength: 255
            },
            product_info: {
                required: true,
                maxlength: 255
            },
            price: {
                required: true,
                digits: true,
                maxlength: 6
            },
            amount: {
                required: true,
                digits: true,
                maxlength: 6
            },
            category_id: {
                required: true,
            },
            product_image: {
                extension: "png|jpe?g|gif|gif",
            }
        },
        messages: {
            product_name: {
                required: "please enter product name",
            },
            product_info: {
                required: "please enter product info"
            },
            price: {
                required: "please enter price"
            },
            amount: {
                required: "please enter amount"
            },
            category_id: {
                required: "please enter category"
            },
            product_image: {
                extension: "Not an image!"
            }
        }
    });
});
