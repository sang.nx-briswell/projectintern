
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php if(isset($user)) { ?>
            <?= $this->fetch('title') ?>
        <?php } ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css">

    <?= $this->Html->css('milligram.min.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('common.css') ?>
    <?= $this->Html->css('bootstrap-grid.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <style>
        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 550px;
                      }
        .col-sm-3.sidenav.hidden-xs {
            margin-left: 20px;
            margin-right: 20px
        }
        /* Set gray background color and 100% height */
        .sidenav {
            background-color: #f1f1f1;
            height: 100%;
        }

        /* On small screens, set height to 'auto' for the grid */
        @media screen and (max-width: 767px) {
            .row.content {height: auto;}
        }
    </style>
</head>
<body>
    <nav class="top-nav">
        <div class="top-nav-title">
            <a href="/product"><span>Web</span>Intern</a>
<!--            check user is exist and user is admin-->
            <?php if(isset($user) && $user["user_flag"] == 0) { ?>
            <a href="/user"><span>USER</span></a>
            <a href="/product/list"><span>PRODUCT</span></a>
            <?php } ?>
<!--            check user is exist and user is staff-->
            <?php if(isset($user) && $user["user_flag"] == 1) { ?>
            <a href="/product/list"><span>PRODUCT</span></a>
            <?php } ?>
        </div>
        <div class="top-nav-links">
            <span><?= isset($user) ? $user["first_name"] . " "  . $user["last_name"] : "" ?></span>
            <a href="/logout"><?= isset($user) ? "Logout" : "" ?></a>
        </div>
    </nav>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
</body>
</html>
