<html>
<header>

    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.min') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.validate') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('additional-methods') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('web/product/edit') ?>"></script>
</header>
<body>
<div>
    <h2>Edit Product</h2>
    <span class="text-danger">
        <?= $this->Flash->render() ?>
    </span>
    <?php
    if(!empty($product["product_image"])) {
        echo $this->Html->image($product["product_image"], array('height' => '160px', 'width' => '160px'));
    }
    echo $this->Form->create(null, [
        'url' => '#',
        'type' => 'post',
        'id' => 'edit_product',
        'enctype' => 'multipart/form-data'
    ]);
    echo $this->Form->file('product_image', [
        'id' => 'fUpload'
    ]);
    echo $this->Form->control('product_name', [
        'label' => [
            'text' => 'Product name:'
        ],
        'class' => 'form-control',
        'value' => $product["product_name"],
        'placeholder' => 'Enter your product name',
    ]);
    echo $this->Form->control('product_info', [
        'label' => [
            'text' => 'Info:'
        ],
        'class' => 'form-control',
        'value' => $product["product_info"],
        'placeholder' => 'Enter product info',
    ]);
    echo $this->Form->control('price', [
        'label' => [
            'text' => 'price:'
        ],
        'class' => 'form-control',
        'value' => $product["price"],
        'placeholder' => 'Enter price',
    ]);
    echo $this->Form->control('amount', [
        'label' => [
            'text' => 'Amount:'
        ],
        'class' => 'form-control',
        'value' => $product["amount"],
        'placeholder' => 'Enter your amount',
    ]);
    echo $this->Form->control('category_id', [
        'label' => [
            'text' => 'Category'
        ],
        'empty' => '--',
        'value' => $product->category->id-1,
        'option' => $categories
    ]);
    echo $this->Form->submit('Edit', [
        'class' => 'btn btn-info',
    ]);
    echo $this->Form->end();
    ?>
</div>
</body>
</html>
