<link rel="stylesheet" href="<?= $this->Url->css('list_user') ?>">
<div class="row">
<?php foreach ($products as $item) { ?>
    <div class="col-ms-4">
        <div class="product">
            <?php
            if(!empty($item["product_image"])) {
                echo $this->Html->link($this->Html->image($item["product_image"]), [
                    'action' => 'view',
                    $item["id"]
                ], [
                    'escape'=>false,
                    'class'=>'thumbnail'
                ]);
            } else {
                echo $this->Html->image('/img/product/image-null.png', [
                    'escape'=>false,
                    'class'=>'thumbnail'
                ]);
            }
            ?>
            <div class="caption">
                <h2>
                    <?php echo $item["product_name"] ?>
                </h2>
                <div class="price">
                    Price: $
                    <?php echo $item["price"] ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</div>
<?php
$paginator = $this->Paginator->setTemplates([
    'number' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'first' => '<li class="page-item"><a href="{{url}}" class="page-link">&laquo;</a></li>',
    'last' => '<li class="page-item"><a href="{{url}}" class="page-link">&raquo;</a></li>',
    'prevActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&lt;</a></li>',
    'nextActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&gt;</a></li>',
]);
?>
<div class="pagination">
    <?php
    echo $paginator->first();
    if($paginator->hasPrev()) {
        echo $paginator->prev();
    }
    echo $paginator->numbers();
    if($paginator->hasNext()) {
        echo $paginator->next();
    }
    echo $paginator->last();
    ?>
</div>
