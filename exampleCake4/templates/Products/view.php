<link rel="stylesheet" href="<?= $this->Url->css('list_user') ?>">
<div class="row">
    <div class="col-ms-4">
        <div class="product">
            <?php echo $this->Html->image($product['product_image']);?>
        </div>
    </div>

    <div class="col-lg-8 col-md-8">
        <div class="caption">
            <h1>
                <?php echo $product['product_name'];?>
            </h1>
            <h3>
                Price: $
                <?php echo $product['price'];?>
            </h3>
        </div>
        <p>
            <?php echo $this->Form->create(null, ['id' => 'add-form', 'url' => '#']);?>
            <?php echo $this->Form->hidden('product_id', ['value' => $product['id']])?>
            <?php echo $this->Form->submit('Add to cart', ['class' => 'btn-success btn btn-lg']);?>
            <?php echo $this->Form->end();?>
        </p>
    </div>
</div>
