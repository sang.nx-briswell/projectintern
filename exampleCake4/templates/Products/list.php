<link rel="stylesheet" href="<?= $this->Url->css('list_user') ?>">
<button type="button" class="btn button-outline">
    <?php
    echo $this->Html->link('Add Product', [
        'controller' => 'products',
        'action' => 'add',
    ])
    ?>
</button>
<?php
$paginator = $this->Paginator->setTemplates([
    'number' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'first' => '<li class="page-item"><a href="{{url}}" class="page-link">&laquo;</a></li>',
    'last' => '<li class="page-item"><a href="{{url}}" class="page-link">&raquo;</a></li>',
    'prevActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&lt;</a></li>',
    'nextActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&gt;</a></li>',
]);
?>
<div class="pagination">
    <?php
    echo $paginator->first();
    if($paginator->hasPrev()) {
        echo $paginator->prev();
    }
    echo $paginator->numbers();
    if($paginator->hasNext()) {
        echo $paginator->next();
    }
    echo $paginator->last();
    ?>
</div>
<?php
echo "<table>
            <tr>
                <td><b>ID</b></td>
                <td><b>Product name</b></td>
                <td><b>Info</b></td>
                <td><b>Price</b></td>
                <td><b>Amount</b></td>
                <td><b>Category</b></td>
                <td></td>
            </tr>";
if(isset($products)) {
    foreach ($products as $item) {
        $id = $item["id"];
        echo "<tr>";
        echo "<td>" . $item["id"] . "</td>";
        echo "<td>" . $item["product_name"] . "</td>";
        echo "<td>" . $item["product_info"] . "</td>";
        echo "<td>" . $item["price"] . "</td>";
        echo "<td>" . $item["amount"] . "</td>";
        echo "<td>" . $item->category["category_name"] . "</td>";
        echo "<td>" . "<a href='/product/edit/$id' target='_blank'><button class='button'>Edit</button></a>" . "</td>";
        echo "</tr>";
    }
}
?>





