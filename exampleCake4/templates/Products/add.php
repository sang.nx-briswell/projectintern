<html>
<header>

    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.min') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.validate') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('additional-methods') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery-ui') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('web/product/add') ?>"></script>
</header>
<body>
<div>
    <h2>Add Product</h2>
    <span class="text-danger">
                <?= $this->Flash->render() ?>
            </span>
    <?php
    echo $this->Form->create(null, [
        'url' => '#',
        'type' => 'post',
        'id' => 'add_product',
        'enctype' => 'multipart/form-data'
    ]);
    echo $this->Form->file('product_image', [
        'id' => 'fUpload'
    ]);
    echo $this->Form->control('product_name', [
        'label' => [
            'text' => 'Product name:'
        ],
        'class' => 'form-control',
        'placeholder' => 'Enter your product name',
    ]);
    echo $this->Form->control('product_info', [
        'label' => [
            'text' => 'Info:'
        ],
        'class' => 'form-control',
        'placeholder' => 'Enter product info',
    ]);
    echo $this->Form->control('price', [
        'label' => [
            'text' => 'price:'
        ],
        'class' => 'form-control',
        'placeholder' => 'Enter price',
    ]);
    echo $this->Form->control('amount', [
        'label' => [
            'text' => 'Amount:'
        ],
        'class' => 'form-control',
        'placeholder' => 'Enter your amount',
    ]);
    echo $this->Form->control('category_id', [
        'label' => [
            'text' => 'Category:'
        ],
        'empty' => '--',
        'option' => $categories,
    ]);
    echo $this->Form->submit('Add', [
        'class' => 'btn btn-info',
    ]);
    echo $this->Form->end();
    ?>
</div>
</body>
</html>
