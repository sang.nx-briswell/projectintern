<html>
    <header>
        <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.min') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.validate') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('additional-methods') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('web/user/user') ?>"></script>
    </header>
    <body>
        <div class="container col-6">
            <h2>
                <?php echo $title?>
            </h2>
            <span class="text-danger">
                <?= $this->Flash->render() ?>
            </span>
                <?php
                echo $this->Form->create(null, [
                    'url' => '/',
                    'type' => 'post',
                    'id' => 'formLogin',
                ]);
                echo $this->Form->control('username', [
                    'label' => [
                        'text' => 'Email:'
                    ],
                    'class' => 'form-control',
                    'type' =>   'email',
                    'placeholder' => 'Enter your email',
                    //array('class required email')
                ]);
                echo $this->Form->control('password', [
                    'label' => [
                        'text' => 'Password:'
                    ],
                    'class' => 'form-control',
                    'type' => 'password',
                    'placeholder' => 'Enter your password',
                ]);
                ?>
                <?php
                echo $this->Form->submit($title, [
                    'class' => 'btn btn-info'
                ]);
                ?>
            <span>You don't have account<a href="/register"><h5>Register now<h5></a></span>
        </div>
    </body>
</html>
