<html>
    <header>

        <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.min') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.validate') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('additional-methods') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery-ui') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('web/product/ajax') ?>"></script>
        <script type="text/javascript" src="<?= $this->Url->script('web/user/edit') ?>"></script>
        <link rel="stylesheet" href="<?= $this->Url->css('jquery-ui') ?>">
    </header>
    <body>
        <div>
            <h2><?php echo $title; ?></h2>
            <span class="text-danger">
                <?= $this->Flash->render() ?>
            </span>
            <?php
            if(!empty($data["avatar"])) {
                echo $this->Html->image($data["avatar"], array('height' => '160px', 'width' => '160px'));
            }
            ?>
            <?php
            echo $this->Form->create(null, [
                'url' => '#',
                'type' => 'post',
                'id' => 'formEdit',
                'enctype' => 'multipart/form-data'
            ]);
            echo $this->Form->file('avatar', [
                'id' => 'fUpload'
            ]);
            echo $this->Form->control('username', [
                'label' => [
                    'text' => 'Email:'
                ],
                'class' => 'form-control',
                'type' => 'email',
                'value' => $data["username"],
                'placeholder' => 'Enter your email',
            ]);
            echo $this->Form->control('password_edit', [
                'label' => [
                    'text' => 'Password:'
                ],
                'class' => 'form-control',
                'type' => 'password',
                //'value' => $data["password"],
                'placeholder' => 'Enter your password',
            ]);
            echo $this->Form->control('first_name', [
                'label' => [
                    'text' => 'First name:'
                ],
                'class' => 'form-control',
                'type' => 'text',
                'value' => $data["first_name"],
                'placeholder' => 'Enter your first name',
            ]);
            echo $this->Form->control('last_name', [
                'label' => [
                    'text' => 'Last name:'
                ],
                'class' => 'form-control',
                'type' => 'text',
                'value' => $data["last_name"],
                'placeholder' => 'Enter your last name',
            ]);
            $date = date("Y-m-d", strtotime($data["birth"]));
            echo  $this->Form->control('birth', [
                'label' => [
                    'text' => 'Birthday:'
                ],
                'class' => 'form-control',
                'type' => 'text',
                'value' => $date,
                'placeholder' => 'Choose birthday',
                'id' => 'birth'
            ]);
            echo $this->Form->control('phone', [
                'label' => [
                    'text' => 'Phone:'
                ],
                'class' => 'form-control',
                'type' => 'text',
                'value' => $data["phone"],
                'placeholder' => 'Enter your phone number',
            ]);
            echo $this->Form->control('city', [
                'id' => 'cities_field',
                'empty' => '--',
                'value' => $data['city'],
                'options' => $cities
            ]);
            echo $this->Form->control('district', [
                'id' => 'districts_field',
                'empty' => '--',
                'value' => $data['district'],
                'options' => $districts
            ]);
            echo $this->Form->hidden('get_country_url', [
                'id' => 'get_country_url',
                'value' => $this->Url->build(['controller' => 'Users', 'action' => 'ajaxDistrictByCity'])
            ]);
            echo $this->Form->control('address', [
                'label' => [
                    'text' => 'Address:'
                ],
                'type' => 'text',
                'class' => 'form-control',
                'value' => $data["address"],
                'placeholder' => 'Enter your address',
            ]);
            echo $this->Form->submit($title, [
                'class' => 'btn btn-info',
            ]);
            echo $this->Form->end();
            ?>
        </div>
    </body>
</html>
