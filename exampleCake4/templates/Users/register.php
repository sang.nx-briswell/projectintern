<header>
    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.min') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.validate') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('additional-methods') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('web/user/register') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('web/product/ajax') ?>"></script>
</header>
<body>
    <div>
        <h2><?php echo $title; ?></h2>
        <span class="text-danger">
            <?= $this->Flash->render() ?>
        </span>
        <?php
            echo $this->Form->create(null, [
                'url' => '/register',
                'type' => 'post',
                'id' => 'formRegister'
            ]);
            echo $this->Form->control('username', [
                'label' => [
                        'text' => 'Email:'
                ],
                'class' => 'form-control',
                'type' => 'email',
                'placeholder' => 'Enter your email'
            ]);
            echo $this->Form->control('password', [
                'label' => [
                        'text' => 'Password:'
                ],
                'class' => 'form-control',
                'type' => 'password',
                'placeholder' => 'Enter your password'

            ]);
            echo $this->Form->control('first_name', [
                'label' => [
                        'text' => 'First name:'
                    ],
                'class' => 'form-control',
                'type' => 'text',
                'placeholder' => 'Enter your first name'
            ]);
            echo $this->Form->control('last_name', [
                'label' => [
                        'text' => 'Last name:'
                ],
                'class' => 'form-control',
                'type' => 'text',
                'placeholder' => 'Enter your last name'
            ]);
            echo $this->Form->control('phone', [
                'label' => [
                        'text' => 'Phone:'
                ],
                'class' => 'form-control',
                'type' => 'text',
                'placeholder' => 'Enter your phone number'
            ]);
            echo $this->Form->control('city', [
                'id' => 'cities_field',
                'empty' => '--',
                'options' => $cities
            ]);
            echo $this->Form->control('district', [
                'id' => 'districts_field',
                'empty' => '--',
                'options' => $districts
            ]);
            echo $this->Form->hidden('get_country_url', [
                'id' => 'get_country_url',
                'value' => $this->Url->build(['controller' => 'Users', 'action' => 'ajaxDistrictByCity'])
            ]);
            echo $this->Form->control('address', [
                'label' => [
                        'text' => 'Address:'
                ],
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'Enter your address'
            ]);
            echo $this->Form->submit($title, [
                'class' => 'btn btn-info'
            ]);
        ?>
    </div>
</body>
