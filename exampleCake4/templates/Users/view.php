<html>
    <header>

    </header>
    <body>
        <div>
            <h2>
                <?php echo $title?>
            </h2>
            <span class="text-danger">
                <?= $this->Flash->render() ?>
            </span>
        </div>
        <div>
            <?php if(!empty($user["avatar"])) {
                echo $this->Html->image($user["avatar"], array('height' => '160px', 'width' => '160px'));
            }
            ?>
            <h3><b>Email: </b><?php echo $user["username"]?></h3><br>
            <h3><b>Name: </b><?php echo $user["first_name"] . " " . $user["last_name"]?></h3><br>
            <h3><b>Birth: </b><?php if(isset($user["birth"])) {
                echo date("Y/m/d", strtotime($user["birth"]));
            }
            ?>
            </h3><br>
            <h3><b>Phone Number: </b><?php echo $user["phone"]?></h3><br>
            <h3><b>Address: </b><?php echo $user["address"]?></h3>
        </div>
    </body>
</html>


