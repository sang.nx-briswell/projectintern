<header>
    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.min') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('jquery/jquery.validate') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('additional-methods') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('web/user/list') ?>"></script>
    <script type="text/javascript" src="<?= $this->Url->script('web/user/ajax-clear-form-seach') ?>"></script>
    <link rel="stylesheet" href="<?= $this->Url->css('list_user') ?>">
</header>
<body>
    <div class="container">
        <?php
            echo "<table>
            <tr>
                <td><b>ID</b></td>
                <td><b>Email</b></td>
                <td><b>First name</b></td>
                <td><b>Last name</b></td>
                <td><b>Phone</b></td>
                <td><b>Address</b></td>
                <td><b>Amount</b></td>
                <td></td>
                <td></td>
            </tr>";
            if(isset($users)) {
                foreach ($users as $item) {
                    $id = $item["id"];
                    echo "<tr>";
                    echo "<td>" . $item["id"] . "</td>";
                    echo "<td>" . $item["username"] . "</td>";
                    echo "<td>" . $item["first_name"] . "</td>";
                    echo "<td>" . $item["last_name"] . "</td>";
                    echo "<td>" . $item["phone"] . "</td>";
                    echo "<td>" . $item["address"] . "</td>";
                    if(isset($item->ord_dt["total"])) {
                        echo "<td>" . $item->ord_dt["total"] . "</td>";
                    } else
                    {
                        echo "<td>" . "</td>";
                    }
                    echo "<td>" . "<a href='user/view/$id' target='_blank'><button class='button'>View</button></a>" . "</td>";
                    echo "<td>" . "<a href='user/edit/$id' target='_blank'><button class='button'>Edit</button></a>" . "</td>";
                    echo "</tr>";
                }
        }
        ?>

        <div class="row">
            <?php
            echo "<h2>" . "List User Info" . "</h2>" . "<br>";
            ?>
            <?php
            echo $this->Form->create(null, [
                'url' => '#',
                'type' => 'post',
                'id' => 'formSearch',
                'enctype' => 'multipart/form-data'
            ]);
            ?>
            <div>
            <?php
                echo $this->Form->control("first_name", [
                    'label' => [
                        'text' => 'Name'
                    ],
                    'class' => 'form-control',
                    'type' => 'text',
                    'value' => isset($searchData['first_name']) ? $searchData['first_name'] : '',
                    'placeholder' => 'Search..'
                ]);
                 echo $this->Form->control("amount_from", [
                    'label' => [
                        'text' => 'Amount from'
                    ],
                    'class' => 'form-control',
                    'type' => 'text',
                     'value' => isset($searchData['amount_from']) ? $searchData['amount_from'] : '',
                    'placeholder' => 'Search..'
                ]);
            ?>
            </div>
            <div>
                <?php
                echo $this->Form->control("email", [
                    'label' => [
                        'text' => 'Email'
                    ],
                    'class' => 'form-control',
                    'type' => 'text',
                    'value' => isset($searchData['email']) ? $searchData['email'] : '',
                    'placeholder' => 'Search..'
                ]);
                echo $this->Form->control("amount_to", [
                    'label' => [
                        'text' => 'Amount to'
                    ],
                    'class' => 'form-control',
                    'type' => 'text',
                    'value' => isset($searchData['amount_to']) ? $searchData['amount_to'] : '',
                    'placeholder' => 'Search..'
                ]);
                ?>
            </div>
            <input type="file" name="upload_file" id="files" style="display:none"/>
            <div class="button-submit">
                <?php
                echo $this->Form->submit("Search", [
                    'class' => 'btn btn-primary'
                ]);
                ?>
                <button type="button" id="clear">Clear</button>
            </div>

        </div>
    </div>
    <div class="portcsv">
        <button type="button" class="btn button-outline" onclick="click_the_button(files);">Import Csv</button>
        <button class="btn button-outline" type="button">
            <?= $this->Html->link('Export CSV', array(
                'controller' => 'Users',
                'action' => 'export',
                'ext' => 'csv',
            ));
            ?>
        </button>
    </div>

    <?php
    $paginator = $this->Paginator->setTemplates([
        'number' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
        'current' => '<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
        'first' => '<li class="page-item"><a href="{{url}}" class="page-link">&laquo;</a></li>',
        'last' => '<li class="page-item"><a href="{{url}}" class="page-link">&raquo;</a></li>',
        'prevActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&lt;</a></li>',
        'nextActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&gt;</a></li>',
    ]);
    ?>
    <div class="pagination">
        <?php
        echo $paginator->first();
        if($paginator->hasPrev()) {
            echo $paginator->prev();
        }
        echo $paginator->numbers();
        if($paginator->hasNext()) {
            echo $paginator->next();
        }
        echo $paginator->last();
        ?>
    </div>
</body>
