<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class UpdateAddressUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('city', 'string', [
            'null' => true,
            'limit' => 256
        ])->addIndex(['address']);
        $table->addColumn('district', 'string', [
            'null' => true,
            'limit' => 256
        ])->addIndex(['city']);
        $table->update();
    }
}
