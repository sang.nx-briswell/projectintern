<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Categories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('categories' , ['id' => false]);
        $table->addColumn('id', 'biginteger', [
            'identity' => true
        ]);
        $table->addPrimaryKey('id');
        $table->addColumn('category_name', 'string', [
            'limit' => 256,
            'null' => false
        ]);
        $table->addColumn('created_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('created_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('updated_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('updated_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_at', 'datetime', [
            'null'=>true,
        ]);

        $table->create();
    }
}
