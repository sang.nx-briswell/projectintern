<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Order extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('orders', ['id' => false]);
        $table->addColumn('id', 'biginteger', [
            'identity' => true
        ]);
        $table->addPrimaryKey('id');
        $table->addColumn('note', 'string', [
            'limit' => 256,
            'null' => true
        ]);
        $table->addColumn('user_id', 'biginteger', [
            'null' => false
        ])->addForeignKey('user_id', 'users', 'id');
        $table->addColumn('created_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('created_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('updated_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('updated_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_at', 'datetime', [
            'null'=>true,
        ]);
        $table->create();
    }
}
