<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class OrderDetail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('order_details', ['id' => false]);
        $table->addColumn('id', 'biginteger', [
            'identity' => true,
        ]);
        $table->addPrimaryKey('id');
        $table->addColumn('amount', 'integer', [
            'null' => false
        ]);
        $table->addColumn('total', 'integer', [
            'null' => false
        ]);
        $table->addColumn('product_id', 'biginteger', [
            'null' => false
        ])->addForeignKey('product_id', 'products', 'id');
        $table->addColumn('order_id', 'biginteger', [
            'null' => false
        ])->addForeignKey('order_id', 'orders', 'id');
        $table->addColumn('created_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('created_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('updated_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('updated_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_at', 'datetime', [
            'null'=>true,
        ]);
        $table->create();
    }
}
