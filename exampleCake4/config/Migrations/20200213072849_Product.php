<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Product extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('products', ['id' => false]);
        $table->addColumn('id', 'biginteger', ['identity' => true]);
        $table->addPrimaryKey('id');
        $table->addColumn('product_name', 'string', [
            'limit' => 256,
            'null' => false
        ]);
        $table->addColumn('product_info', 'string', [
            'limit' => 256,
            'null' => false
        ]);
        $table->addColumn('product_image', 'string', [
            'limit' => 256,
            'null' => false
        ]);
        $table->addColumn('price', 'integer', [
            'null' => true
        ]);
        $table->addColumn('amount', 'integer', [
            'null' => true
        ]);
        $table->addColumn('top_flg', 'integer', [
            'default'=>1,
            'limit'=>\Phinx\Db\Adapter\MysqlAdapter::INT_SMALL,
        ]);
        $table->addColumn('category_id', 'biginteger', [
            'null' => false
        ])->addForeignKey('category_id', 'categories', 'id');
        $table->addColumn('created_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('created_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('updated_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('updated_at', 'datetime', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_by', 'biginteger', [
            'null'=>true,
        ]);
        $table->addColumn('deleted_at', 'datetime', [
            'null'=>true,
        ]);
        $table->create();

    }
}
